//Modify this file to change what commands output to your statusbar, and recompile using the make command.

#define SP(path) "$HOME/Suckless/dwmblocks/scripts/" #path

static const Block blocks[] = {
	/*Icon*/	/*Command*/		    /*Update Interval*/	      /*Update Signal*/
	{"",        SP("player.sh"),    2,	                      10},
    {"",        SP("volume.sh"),    2,                        10},
	{"",        SP("date.sh"),      3600,                      0},
	{"",        SP("time.sh"),    	30,	                       0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
