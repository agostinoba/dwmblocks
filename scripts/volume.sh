#!/bin/sh

echo $(amixer get Master | grep -m1 % | awk '{ print $4 }' | tr -d '[]')
