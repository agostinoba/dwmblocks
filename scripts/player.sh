#!/bin/bash

# Versión solo de Play

if [ 'Playing' = "$(playerctl --player=spotify status)" ]; then
    echo $(playerctl --player=spotify metadata title) - $(playerctl --player=spotify metadata artist)
fi

if [ 'Playing' = "$(playerctl --player=vlc status)" ]; then
    echo $(playerctl --player=vlc metadata title) - $(playerctl --player=vlc metadata artist)
fi

#####################################################################################################

# Verisón con Play y Pause

#if [[ ( 'Playing' = "$(playerctl --player=spotify status)" )  || ( 'Paused' = "$(playerctl --player=spotify status)" && 'Playing' != "$(playerctl --player=vlc status)" ) ]]; then
#    echo $(playerctl --player=spotify metadata title) - $(playerctl --player=spotify metadata artist)
#fi
#
#if [[ ( 'Playing' = "$(playerctl --player=vlc status)" )  || ( 'Paused' = "$(playerctl --player=vlc status)" && 'Playing' != "$(playerctl --player=spotify status)" ) ]]; then
#    echo $(playerctl --player=vlc metadata title) - $(playerctl --player=vlc metadata artist)
#fi
